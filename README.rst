.. _README:

Get Stock Prices
================

Requirements
------------

* Python3 & Pip

  .. code-block::

    $ apt-get install python3 python3-pip

* iexfinance package

  .. code-block::

    $ pip install iexfinance

* https://iexcloud.io/ free account

* Export your token from you account

  .. code-block::

    $ export IEX_TOKEN=$IEXCLOUD_API_TOKEN

* ``./stock_list.txt``

  Put your stock identifiers in this file. One identifier per line

  For example:

  .. code-block:: text

     AAPL
     GOOG
     MSFT

How To Run
----------

#. Find the stocks you want to watch

   * Using this script you should be able to watch about 40 stocks per trading day.
   * You can search IEX Cloud's list on their console web page.  There is a search
     box at the top of the page.

#. Setup your ``stock_list.txt`` file

#. Run ``get_stock_prices`` after the market has closed for the day.

   * Don't be afraid to run this on non-market days, since it returns nothing no
     messages will be spent.

#. Review all the price history in the prices directory.

   * Each stock will be stored in its own directory with a file name which matches
     market date in which it references.


FYI
---

``get_historical_data(..)``
  This function will eat through your core messages in a heart beat. One call that
  asked for four days worth of prices ate through 1-4k messages (of your 50K allotment).
  I fear They have a bug...

``get_quote()``
  **Only use this if you are using the data for real time tracking.** There
  are better ways if you are trying to gather the data for historical tracking.

  This will return a quote at the moment you run. But this is by far not the
  most efficient usage of your core messages. It tasks one message to get a
  current quote. So for a single stock, if you wanted to get a quote every minute
  it would cost you 510/units per day. With a max of 50000 units in a month, you
  can only pull in ~4 stocks a month with this method.

  I even tried to use ``get_quote(["AAPL","GOOG"], ...)`` to see if you could
  combine things and it didn't help.

``get_historical_intraday($STOCK, $DATE, ...)``
  **Use this if you are looking for a daily minute by minute price after the
  market has closed.**

  This will return the stock price every minute, for the date provided (Up to
  three months old, or today if date is empty). In terms of cost, it will cost
  you 50 units per stock per day. So if you are willing to wait until the end
  of the day to get a minute by minute recording, you can increase the number of
  stocks you track by an order of magnitude. This will allow you to track ~43
  different stocks, over the course of the month and get a minute by minute
  quote.

ToDo
----

These are the thing that need to be done.

.. code-block:: text

  ○ See if there is an API call to tell how many API calls have been made this month.
    - I have code in there but commented out as it is not working at the moment.
  ✗ Create a CronJob which only runs when the market is open
    - Because we can get a minute by minute change, at the end of the day, and becuase it costs 0 messages
      when the market is closed there is no need to care... Just have the cron run everyday after the market closes.
    ✗ It should run every 5 mintes.
  ✓ Determine with 10 stocks you want to watch over time.
    ✗ Look at stocks which are volitile, and some that are steady.
    - I just went looking for random things that sound interesting.
  ✓ Make it so that a new file is created every day.
  ✓ Make it so the Executable loops through a list of stocks read in from a file.
  ✓ Remove the API key from the executable. Put it in a hidden file which is read in at run time.
    - Just use the API referenceable VAR seen above.
  ✓ write up a README, to include how to set up the environment.
  ✓ Commit the code.
  □ Write a script to parse the data into a csv.
  □ Write a cron job to run this every day.
